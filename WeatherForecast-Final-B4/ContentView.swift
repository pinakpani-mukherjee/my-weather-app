//
//  ContentView.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var cityAdded = ""
    var body: some View {
        ZStack {
             BackSplash()
            
            TopView(cityAdded: self.$cityAdded)
        }
       
       
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
