//
//  WeatherStore.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/26/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI
import Combine

class WeatherStore: BindableObject {
    var willChange = PassthroughSubject<WeatherStore, Never>()
    
    var weather: [TopLevel] = [] {
        willSet {
            willChange.send(self)
        }
    }
    
    let service: WeatherService
    init(service: WeatherService) {
        self.service = service
    }
    
    func fetch(matching query: String) {
        service.search(matching: query) {
            [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let weather): self?.weather = [weather]
                case .failure : self?.weather = []
                
                    
                }
                print("Query: \(query)")
                print("Res ==> \(result)")
            }
        }
    }
}
