//
//  Menu.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct Menu: Identifiable {
    var id = UUID()
    var image: String = "map.fill"
    var tag: String = "tag"
}
