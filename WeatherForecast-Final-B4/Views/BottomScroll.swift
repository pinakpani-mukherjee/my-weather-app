//
//  BottomScroll.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct BottomScroll: View {
    let weatherData: List
    @State var icon = "sun.max"
    
    var body: some View {
        ZStack {
            ZStack(alignment: .topLeading) {
                Text(String(Helper().timeConverter(timeStamp: weatherData.dt, isDay: true)))
                    .foregroundColor(Color("icons"))
            }.offset(y: -75)
            
            HStack {
                Image(systemName: Helper().showWeatherIcon(item: weatherData.weather.first!))
                    .resizable()
                    .frame(width: 60, height: 60)
                    .foregroundColor(Color("secondary"))
                    .background(RoundedRectangle(cornerRadius: 60)
                        .frame(width: 90, height: 80)
                        .foregroundColor(Color("gradient2")))
                    .padding(.all, 20)
                    .offset(x: -20)
                
                VStack(alignment: .leading, spacing: 5) {
                    HStack {
                        Text(String(format: "%.0f", weatherData.temp.min) + "º")
                            .foregroundColor(.gray)
                        
                        Image(systemName: "arrow.down")
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                        Text(String(format: "%.0f", weatherData.temp.max) + "º")
                            .foregroundColor(.gray)
                        
                        Image(systemName: "arrow.up")
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(.gray)
                    }
                    Text("Hum: \(String(format: "%.0f",weatherData.temp.min))%")
                        .foregroundColor(.gray)
                        .font(.subheadline)
                    Text("Win: \(String(format: "%.1f",weatherData.speed)) mi/h")
                        .foregroundColor(.gray)
                        .font(.subheadline)
                    
                }
                Spacer()
            }
            
            
        }.frame(width: 220, height: 200)
            .background(Color.white)
            .cornerRadius(30)
            .padding(.leading, 15)
    }
    
   
       
}

//#if DEBUG
//struct BottomScroll_Previews: PreviewProvider {
//    static var previews: some View {
//        BottomScroll()
//    }
//}
//#endif
