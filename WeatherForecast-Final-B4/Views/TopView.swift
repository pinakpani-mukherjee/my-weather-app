//
//  TopView.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

let UIWidth = UIScreen.main.bounds.width
let UIHeight = UIScreen.main.bounds.height
struct TopView: View {
    @Binding var cityAdded: String
    @State var placeHolder = "Enter city"
    @State var showField: Bool = false
    
    @EnvironmentObject var weatherStore: WeatherStore
    
    var body: some View {
        ZStack {
            ZStack(alignment: .leading) {
                TextField(self.$cityAdded, placeholder: Text(self.placeHolder)) {
                     //oncommit
                    self.processView()
                    
                }.padding(.all, 10)
                    .frame(width: UIWidth - 50, height: 50)
                 .background(Color("lightBlue"))
                 .cornerRadius(30)
                 .foregroundColor(.white)
                    .offset(x: self.showField ? 0 : (-UIWidth / 2 - 180))
                 .animation(.spring())
                
                Image(systemName: "magnifyingglass.circle.fill")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .foregroundColor(.white)
                    .offset(x: self.showField ? (UIWidth - 90) : -30)
                    .animation(.spring())
                    .tapAction {
                            self.showField.toggle()
                }
                
            }.onAppear(perform: fetch)
        }
    }
    
    func processView()  {
            self.showField = false
            fetch()
          //self.weatherStore.fetch(matching: self.cityAdded)
    }
    
    private func fetch() {
         self.weatherStore.fetch(matching: self.cityAdded)
    }
}

//#if DEBUG
//struct TopView_Previews: PreviewProvider {
//    static var previews: some View {
//        TopView()
//    }
//}
//#endif
