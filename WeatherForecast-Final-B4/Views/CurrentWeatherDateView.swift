//
//  CurrentWeatherDateView.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/26/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct CurrentWeatherDateView: View {
    let weatherData: TopLevel
    
    var body: some View {
        HStack {
            Text(String(Helper().timeConverter(timeStamp: weatherData.list.first!.dt, isDay: false)))
                .font(.system(size: 15))
                .foregroundColor(.white)
                .bold()
        }
    }
    
   
}

//#if DEBUG
//struct CurrentWeatherDateView_Previews: PreviewProvider {
//    static var previews: some View {
//        CurrentWeatherDateView()
//    }
//}
//#endif
