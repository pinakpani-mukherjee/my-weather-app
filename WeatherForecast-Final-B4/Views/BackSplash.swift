//
//  BackSplash.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct BackSplash: View {
    var body: some View {
        Rectangle()
            .fill(LinearGradient(gradient: Gradient(colors: [Color("lightPink"), Color("lightBlue")]),
                                   startPoint: .top,
                                   endPoint: .bottom))
                .edgesIgnoringSafeArea(.all)
             
    }
}

#if DEBUG
struct BackSplash_Previews: PreviewProvider {
    static var previews: some View {
        BackSplash()
    }
}
#endif
