//
//  MidView.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct MidView: View {
    
    @EnvironmentObject var weatherStore: WeatherStore
    
    @Binding var cityText: String
    
    var body: some View {
        VStack(alignment: .leading) {
            VStack {
                VStack {
                    
                    ForEach(weatherStore.weather, id: \.cod) { item in
                        
                        self.getCountryAndCity(item: item)
                        
                        CurrentWeatherDateView(weatherData: item)
                        
                    }
                    
                    
                }.padding(.trailing, 200)
                    .frame(width: 400)
                
                
                Spacer()
                
            }
            
            //Icon
            HStack(alignment: .center, spacing: 8) {
                
                ForEach(weatherStore.weather, id: \.cod) { item in
                    Image(systemName: Helper().showWeatherIcon(item: item))
                                       .resizable()
                                       .foregroundColor(.white)
                }
               
                
                
            }.frame(width: (UIWidth / 2) + 90,
                    height: (UIHeight / 2) - 150)
                .padding(.leading, 60)
            
            HStack {
                Spacer()
                //CurrentTempView
                ForEach(weatherStore.weather, id: \.cod) { item in
                    
                    CurrentTempView(weatherData: item)
                    
                }
                
                Spacer()
                
                
            }
            
            Text("7-Day Forecast")
                .foregroundColor(.white)
                .bold()
                .padding(.bottom, 20)
                .padding(.leading, 150)
            
            Spacer()
            
        }
        
        
    }
    
    func getCountryAndCity(item: TopLevel) -> some View {
        
        let countryAndCity = String("\(item.city.name), \(item.city.country)")
        return VStack(alignment: .leading) {
            Text(countryAndCity)
                .foregroundColor(.white)
                .font(.title)
                .fontWeight(.light)
                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                .lineLimit(nil)
            
        }.padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
    }
}

//#if DEBUG
//struct MidView_Previews: PreviewProvider {
//    static var previews: some View {
//        MidView()
//    }
//}
//#endif
