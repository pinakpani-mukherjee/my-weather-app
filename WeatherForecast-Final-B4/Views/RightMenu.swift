//
//  RightMenu.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

let menuData: [Menu] = [
   Menu( image: "map.fill", tag: "map"),
   Menu( image: "info.circle.fill", tag: "info"),
   Menu( image: "ellipsis.circle", tag: "more"),
]
struct RightMenu: View {
    @Binding var showView: Bool

    var dataMenu = menuData

    var body: some View {
        ZStack(alignment: .topTrailing) {
            VStack {
                ForEach(dataMenu) { item in
                    MenuItem(icon: item.image)
                        .tapAction {
                            if item.tag == "map"{
                                print("Map tapped")
                            }else if item.tag == "info" {
                                print("Info tapped")
                            }else {
                                 print("More was tapped")
                            }
                    }
                }
//                Image(systemName: "map")
//                    .resizable()
//                    .frame(width: 50, height: 50)
                
            }.frame(width: 100, height: UIWidth / 2)
            .background(Color("lightBlue"))
            .cornerRadius(30)
            .foregroundColor(.white)
                .offset(x: self.showView ? 30 : (UIWidth / 2) - 120)
            .animation(.spring())
            .shadow(color: Color("accentShadow"), radius: 10, x: 0, y: 10)
                .tapAction {
                    self.showView.toggle()
            }
            Spacer()
            
        }.padding(.top, 30)
        .padding(.all, 20)
            .frame(width: UIWidth)
    }
}

//#if DEBUG
//struct RightMenu_Previews: PreviewProvider {
//    static var previews: some View {
//        RightMenu()
//    }
//}
//#endif
