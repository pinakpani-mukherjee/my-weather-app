//
//  Home.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct Home: View {
    @State var cityAdded = "Lisbon"
    @State var showView: Bool = false
    
    @EnvironmentObject var weatherStore: WeatherStore
    

    
    var body: some View {
        ZStack {
            BackSplash()
            VStack {
                TopView(cityAdded: self.$cityAdded, weatherStore: self._weatherStore)
                //TopView(cityAdded: self.$cityAdded)
                
                MidView(cityText: self.$cityAdded)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(weatherStore.weather, id: \.cod) { item in
                            ForEach(item.list, id: \.dt) { forecast in
                                 BottomScroll(weatherData: forecast)
                            }
                        
                              
                        }
                    }
                }
                
                
            }
            RightMenu(showView: self.$showView)
            
        }
        
    }
}

#if DEBUG
struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
#endif
