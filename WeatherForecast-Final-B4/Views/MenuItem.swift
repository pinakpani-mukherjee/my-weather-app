//
//  MenuItem.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/25/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct MenuItem: View {
    var icon = "map.fill"
    var body: some View {
        HStack {
            
             Image(systemName: icon)
                //.resizable()
                //.frame(width: 35, height: 35)
                .font(.largeTitle)
                .padding(.bottom, 20)
            
        }
        
    }
}

#if DEBUG
struct MenuItem_Previews: PreviewProvider {
    static var previews: some View {
        MenuItem()
    }
}
#endif
