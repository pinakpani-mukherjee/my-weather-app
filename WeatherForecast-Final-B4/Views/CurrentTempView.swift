//
//  CurrentTempView.swift
//  WeatherForecast-Final-B4
//
//  Created by Paulo Dichone on 7/26/19.
//  Copyright © 2019 Paulo Dichone. All rights reserved.
//

import SwiftUI

struct CurrentTempView: View {
    let weatherData: TopLevel
    
    var body: some View {
        VStack {
            HStack {
                Text(String(format: "%.0f",(weatherData.list.first?.temp.day)!) + "º") //90
                    .font(.system(size: 45))
                    .foregroundColor(.white)
                    .bold()
                
                Text(String((weatherData.list.first?.weather.first?.weatherDescription.capitalized(with: .current))!))
                    .foregroundColor(Color("secondary"))
                
            }
            HStack(spacing: 6) {
                VStack{
                    Text(String(format: "%.0f",(weatherData.list.first?.speed)!) + " mi/h")
                        .bold()
                        .foregroundColor(Color("secondary"))
                    Text("Wind")
                        .foregroundColor(Color("secondary"))
                }
                
                VStack{
                    Text(String((weatherData.list.first?.humidity)!) + "%")
                        .bold()
                        .foregroundColor(Color("secondary"))
                    Text("Humidity")
                        .foregroundColor(Color("secondary"))
                }
                
                VStack{
                    Text(String(format: "%.0f",(weatherData.list.first?.temp.max)!) +
                    "º")
                        .bold()
                        .foregroundColor(Color("secondary"))
                    Text("Max")
                        .foregroundColor(Color("secondary"))
                }
            }
        }
    }
}
//
//#if DEBUG
//struct CurrentTempView_Previews: PreviewProvider {
//    static var previews: some View {
//        CurrentTempView()
//    }
//}
//#endif
